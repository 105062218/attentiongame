var menuState = {
    preload: function () {
        game.load.image('bgMenu', 'assets/menu.png');
        game.load.bitmapFont('carrier_command', 'assets/carrier_command.png', 'assets/carrier_command.xml');

        //preload of game.js
        game.load.spritesheet('player', 'assets/player.png', 32, 32);
        game.load.image('wall', 'assets/wall.png');
        game.load.image('ceiling', 'assets/ceiling.png');
        game.load.image('normal', 'assets/normal.png');
        game.load.image('nails', 'assets/nails.png');
        game.load.spritesheet('conveyorRight', 'assets/conveyor_right.png', 96, 16);
        game.load.spritesheet('conveyorLeft', 'assets/conveyor_left.png', 96, 16);
        game.load.spritesheet('trampoline', 'assets/trampoline.png', 96, 22);
        game.load.spritesheet('fake', 'assets/fake.png', 96, 36);
        game.load.image('life_1', 'assets/life_1.png');
        game.load.image('life_2', 'assets/life_2.png');
        game.load.image('life_3', 'assets/life_3.png');
        game.load.image('life_4', 'assets/life_4.png');
        game.load.image('life_5', 'assets/life_5.png');
        game.load.image('heart', 'assets/heart.png');
        game.load.image('teacher', 'assets/teacher.png');
        game.load.image('ghost', 'assets/ghost.png');
        game.load.audio('sound_jump', ['assets/jump.wav', 'assets/jump.mp3']);
        game.load.audio('sound_hurt', ['assets/hurt.wav', 'assets/hurt.mp3']);
        game.load.audio('sound_power', ['assets/powerup.wav', 'assets/powerup.mp3']);
        game.load.image('bgGame', 'assets/bgGame.jpg');     //background

        //preload of over.js
        game.load.image('bgOver', 'assets/over.jpg');

        //preload of again.js
        game.load.image('bgAgain', 'assets/again.jpg');
    },

    create: function() {
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;
        
        // Add a background image game.add.image(0, 0, 'background');
        this.bg = game.add.image(60, 0, 'bgMenu');
        this.bg.scale.setTo(2.85, 2.85);

        this.bmpText = game.add.bitmapText(165, 30, 'carrier_command','press Enter to start', 20);
        this.textDisplay = true;
        this.count = 0;
        this.bmpText.inputEnabled = true;
        this.bmpText.input.enableDrag();

        this.keyEnter = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        this.keyEnter.onDown.add(this.start, this);
    },

    update: function(){
        this.count++;
        if((this.count % 45) == 0){
            if(this.textDisplay){
                this.bmpText.kill();
                this.textDisplay = false;
            }  
            else{
                this.bmpText = game.add.bitmapText(165, 30, 'carrier_command','press Enter to start', 20);
                this.textDisplay = true;
            }
        }
    },

    start: function(){
        game.state.start('main');
    }
};