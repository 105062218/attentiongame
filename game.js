var lastTime = 0;
var score = 0;
var n = 1;

var mainState = {
    preload: function() {
    },

    create: function() {
        //game.physics.startSystem(Phaser.Physics.ARCADE);
        this.bg = game.add.tileSprite(0, 10, 1920, 1080, 'bgGame'); //背景图
        this.bg.scale.setTo(0.6, 0.65);

        this.teacher1 = game.add.sprite(200, 200, 'teacher');
        this.teacher1.alpha = 0;
        this.teacher1.scale.setTo(0.5,0.5);
        game.time.events.add(Phaser.Timer.SECOND * 20, function(){game.add.tween(this.teacher1).to( { alpha: 0.5 }, 8000, Phaser.Easing.Linear.None, true);}, this);

        this.teacher2 = game.add.sprite(400, 400, 'teacher');
        this.teacher2.alpha = 0;
        this.teacher2.scale.setTo(0.5,0.5);
        game.time.events.add(Phaser.Timer.SECOND * 50, function(){game.add.tween(this.teacher2).to( { alpha: 0.5 }, 8000, Phaser.Easing.Linear.None, true);}, this);

        this.ghost = game.add.sprite(100, 150, 'ghost');
        this.ghost.alpha = 0;
        this.ghost.scale.setTo(0.5,0.5);
        game.time.events.add(Phaser.Timer.SECOND * 25, function(){game.add.tween(this.ghost).to( { alpha: 1 }, 8000, Phaser.Easing.Linear.None, true);}, this);

        this.ghost1 = game.add.sprite(700, 500, 'ghost');
        this.ghost1.alpha = 0;
        this.ghost1.scale.setTo(0.5,0.5);
        game.time.events.add(Phaser.Timer.SECOND * 40, function(){game.add.tween(this.ghost1).to( { alpha: 1 }, 8000, Phaser.Easing.Linear.None, true);}, this);

        this.ghost2 = game.add.sprite(300, 800, 'ghost');
        this.ghost2.alpha = 0;
        this.ghost2.scale.setTo(0.5,0.5);
        game.time.events.add(Phaser.Timer.SECOND * 55, function(){game.add.tween(this.ghost1).to( { alpha: 1 }, 8000, Phaser.Easing.Linear.None, true);}, this);


        game.time.events.add(Phaser.Timer.SECOND * 50, function(){n=-1;}, this);

        this.createBoundary();
        this.createScoreBoard();
        this.platforms = [];
        this.hearts = [];
        this.createPlayer();
        this.first = true;
    },

    // Call moving function in each frame.
    update: function() {
        game.physics.arcade.collide(this.player, this.leftupWall);
        game.physics.arcade.collide(this.player, this.leftdownWall);
        game.physics.arcade.collide(this.player, this.rightupWall);
        game.physics.arcade.collide(this.player, this.rightdownWall);
        game.physics.arcade.collide(this.player, this.ceiling);
        game.physics.arcade.collide(this.player, this.platforms, this.collidePlatforms, null, this);
        game.physics.arcade.overlap(this.player, this.hearts, this.collidehearts, null, this);

        //game.time.events.add(Phaser.Timer.SECOND * 50, function(){n=-1;}, this);

        this.collideCeiling();

        if (!this.player.inWorld && this.first) {
            this.first = false;
            this.hurtSound.play();
            setTimeout(()=>{
                this.playerDie();
            }, 1500);
        }

        this.createSound();
        this.updateLives();
        this.updatePlatforms();
        this.createPlatforms();
        this.movePlayer();
    },

    createSound: function(){
        this.jumpSound = game.add.audio('sound_jump');
        this.hurtSound = game.add.audio('sound_hurt');
        this.power = game.add.audio('sound_power');
    },

    createPlayer: function(){
        this.player = game.add.sprite(game.width/2, 56, 'player');   
        this.player.scale.setTo(1.4, 1.4);  //大小
        this.player.anchor.setTo(0.5, 0.5); //position_center

        this.cursor = game.input.keyboard.createCursorKeys();   //control player

        game.physics.arcade.enable(this.player);
        this.player.body.gravity.y = 500;   // Add vertical gravity to the player 
        this.player.animations.add('left', [0, 1, 2, 3], 8);
        this.player.animations.add('right', [9, 10, 11, 12], 8);
        this.player.animations.add('flyleft', [18, 19, 20, 21], 12);
        this.player.animations.add('flyright', [27, 28, 29, 30], 12);
        this.player.animations.add('fly', [36, 37, 38, 39], 12);
        this.player.animations.add('hurtLeft', [4, 5, 6, 7], 8);
        this.player.animations.add('hurtRight', [13, 14, 15, 16], 8);
        this.player.animations.add('hurtFlyLeft', [22, 23, 24, 25], 12);
        this.player.animations.add('hurtFlyRight', [31, 32, 33, 34], 12);
        this.player.animations.add('hurtFly', [40, 41, 42, 43], 12);
        this.player.animations.add('hurting', [8, 17], 12);
        this.hurt = false;
    },

    createBoundary: function(){
        //walls
        this.leftupWall = game.add.sprite(0, 0, 'wall');
        game.physics.arcade.enable(this.leftupWall);
        this.leftupWall.body.immovable = true;
        this.leftdownWall = game.add.sprite(0, 300, 'wall');
        game.physics.arcade.enable(this.leftdownWall);
        this.leftdownWall.body.immovable = true;

        this.rightupWall = game.add.sprite(820, 0, 'wall');
        game.physics.arcade.enable(this.rightupWall);
        this.rightupWall.body.immovable = true;
        this.rightdownWall = game.add.sprite(820, 300, 'wall');
        game.physics.arcade.enable(this.rightdownWall);
        this.rightdownWall.body.immovable = true;

        //ceiling
        this.ceiling = game.add.sprite(17, 2, 'ceiling');
        this.ceiling.scale.setTo(2.0, 2.0);
    },

    createScoreBoard: function(){
        var text_style = {fill: '#ffffff', fontSize: '20px'}
        score = 0;
        this.scoreLabel = game.add.text(40, 53, 'Floor :  B' + score, text_style);
        this.addFloor = true;
        this.lastPlat = undefined;

        this.life = 5;
        this.lifeLabel = game.add.text(590, 53, 'Life :', text_style);
        this.lives = game.add.sprite(650, 50, 'life_5');
    },

    updateLives: function(){
        if(this.life < 1){
            this.lives.kill();
            this.playerDie();
        }
        else if(this.life == 1){
            this.lives.kill();
            this.lives = game.add.sprite(650, 50, 'life_1');
        }
        else if(this.life == 2){
            this.lives.kill();
            this.lives = game.add.sprite(650, 50, 'life_2');
        }
        else if(this.life == 3){
            this.lives.kill();
            this.lives = game.add.sprite(650, 50, 'life_3');
        }
        else if(this.life == 4){
            this.lives.kill();
            this.lives = game.add.sprite(650, 50, 'life_4');
        }
        else if(this.life == 5){
            this.lives.kill();
            this.lives = game.add.sprite(650, 50, 'life_5');
        }
    },

    collideCeiling: function(){
        if(this.player.y < 55){
            this.hurt = true;
            this.hurtSound.play();
            setTimeout(function() {
                platform.body.checkCollision.up = false;
            }, 100);
            this.player.y += 2;
            if(this.hurt & this.hurtCeiling){
                this.life--;
                this.hurtCeiling = false;
            }
        }
    },

    playerDie: function() { 
        game.state.start('over');
    },

    movePlayer: function() {
        if(!this.player.body.touching.down)
            this.addFloor = true;
        if(this.player.y > 58)
            this.hurtCeiling = true;
        if(this.cursor.left.isDown){
            this.player.body.velocity.x = -280;
            if(this.player.body.velocity.y == 0){
                if(!this.hurt)
                    this.player.animations.play('left');
                else{
                    this.player.animations.play('hurtLeft');
                    this.hurt = false;
                }
            } 
            else{
                if(!this.hurt)
                    this.player.animations.play('flyleft');
                else{
                    this.player.animations.play('hurtFlyLeft');
                    this.hurt = false;
                }
            }
        }
        else if(this.cursor.right.isDown){
            this.player.body.velocity.x = 280;
            if(this.player.body.velocity.y == 0){
                if(!this.hurt)
                    this.player.animations.play('right');
                else{
                    this.player.animations.play('hurtRight');
                    this.hurt = false;
                }
            }
            else {
                if(!this.hurt)
                    this.player.animations.play('flyright');
                else{
                    this.player.animations.play('hurtFlyRight');
                    this.hurt = false;
                }
            }   
        }
        else {
            this.player.body.velocity.x = 0;
            if(this.player.body.velocity.y != 0){
                if(!this.hurt)
                    this.player.animations.play('fly');
                else{
                    this.player.animations.play('hurtFly');
                    this.hurt = false;
                }
            }
            else{
                if(!this.hurt)
                    this.player.frame = 8;
                else{
                    this.player.animations.play('hurting');
                    this.hurt = false;
                }
            }
        }
    },

    updatePlatforms: function(){    
        this.platforms.forEach(function(item){
            if(item.y < 35)
                item.kill();
            item.y -= 3.7;
        });

        this.hearts.forEach(function(item){
            if(item.y < 35)
                item.kill();
            item.y -= 3.7;
        });
    },

    collidePlatforms: function(player, platform){ 
        if(platform.key == 'normal' && this.player.body.touching.down){
        }
        else if(platform.key == 'nails' && this.player.body.touching.down){
            this.hurt = true;
        }
        else if(platform.key == 'fake'){
            platform.animations.play('turn');
            setTimeout(function() {
                platform.body.checkCollision.up = false;
            }, 100);
            if(this.lastPlat == platform.key)
                this.addFloor = false;
        }
        else if(platform.key == 'conveyorRight'){
            player.x += 2;
        }
        else if(platform.key == 'conveyorLeft'){
            player.x -= 2;
        }
        else if(platform.key == 'trampoline'){
            platform.animations.play('jump');
            player.body.velocity.y = -350;
            this.jumpSound.play();
            if(this.lastPlat == platform.key)
                this.addFloor = false;
        }
        this.lastPlat = platform.key;
        if(this.addFloor && this.player.body.touching.down){
            if(this.hurt){
                this.life -= 1;
                this.hurtSound.play();
            }
            score+=n;
            this.scoreLabel.text = 'Floor :  B' + score;
            this.addFloor = false;
        }
    },

    createPlatforms: function(){
        if(game.time.now > lastTime + 320) {
            lastTime = game.time.now;
            this.createOnePlatform();
            //this.distance += 1;
        }
    },

    createOnePlatform: function(){
        var posX = Math.random()*671 + 50;
        var posY = 700;
        var randomNum = Math.random() * 100;
        var randomNum_h = Math.random() * 20;
        if(randomNum < 5){
            this.heart = game.add.sprite(posX+20, posY-55, 'heart');
            game.physics.arcade.enable(this.heart);
            this.heart.scale.setTo(0.1, 0.1);
            this.heart.body.immovable = true;
            this.hearts.push(this.heart);
        }
        if(randomNum < 50){
            this.platform = game.add.sprite(posX, posY, 'normal');
        }
        else if(randomNum < 60){
            this.platform = game.add.sprite(posX, posY, 'nails');
            game.physics.arcade.enable(this.platform);
            this.platform.body.setSize(96, 15, 0, 15);
        }
        else if(randomNum < 70){
            this.platform = game.add.sprite(posX, posY, 'fake');
            this.platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
        }
        else if(randomNum < 80){
            this.platform = game.add.sprite(posX, posY, 'conveyorRight');
            this.platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            this.platform.animations.play('scroll');
        }
        else if(randomNum < 90){
            this.platform = game.add.sprite(posX, posY, 'trampoline');
            this.platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
            this.platform.frame = 3;
        }
        else if(randomNum < 100){
            this.platform = game.add.sprite(posX, posY, 'conveyorLeft');
            this.platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            this.platform.animations.play('scroll');
        }

        game.physics.arcade.enable(this.platform);
        this.platform.body.immovable = true;

        this.platform.body.checkCollision.down = false;
        this.platform.body.checkCollision.left = false;
        this.platform.body.checkCollision.right = false;

        this.platforms.push(this.platform);
    },

    collidehearts: function(player, heart){
        heart.kill();
        this.power.play();
        if(this.life < 5)
            this.life++;
        else
            this.life = 5;
    }
};

///Initialize Phaser
var game = new Phaser.Game(837, 700, Phaser.AUTO, "canvas"); 